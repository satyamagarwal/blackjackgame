# Not exactly a README, but summary of the thoughts on the things I did, and the way I did #

### General Idea/Challenge ###

* With this project, I wanted to try if I can complete it without any framework and without mutation.
* I used the bottom up approach to build the game, which here, helped me breaking down all the rules and features separately and take one feature at a time. Make methods for each feature, and test it with all possible scenarios(humanly :P). In the end, all method was very easy to weave together.

# NOTE #

* If you are only interested in how BlackJack two player looks like, revert last three commits (or checkout [eba26f2](https://bitbucket.org/satyamagarwal/blackjackgame/commits/eba26f2374cf2fe15338fd1d0a345dad91a6c01e)).
* This is the commit, where I think, blackJack two player is complete. Of course, if you look at the commit just above it, there, refactoring is much more. But I feel, for two players, scenarios are little different, and there is no need for unnecessary optimizations (writing code thinking about what if I have to scale it in the future), unless, the business specifically tells you that.

# If you are interested in reading on, please do :D #

### Idea behind using vavr and AssertJ ###

* AssertJ let me write the assertions with much better readability.
* The reason I picked `vavr` was it gives you play with streams a little better than Java's. Also, it give more readability, and more APIs to play with streams with immutability.
* All the `vavr's APIs` that I used in this projects are convertible to Java's stream (almost, exception being : `Collection.zipWith` (don't know if its there in Java)). 
* All the `vavr's APIs` that I used here, I am not by far good at them. I used most of them for the first time, and read about them just enough, to take advantage from them. 
* I am planning to read more about it in next week or so, they are really nice APIs.

# More about commits #

### [Connect all the methods to complete the game loop. * Add tests * small refactoring](https://bitbucket.org/satyamagarwal/blackjackgame/commits/43247684a0c6c72ff6b15d53da639d02c108e281) ###

* With this commit, I was able to complete the basic flow.

### [Replace recursion with iteration](https://bitbucket.org/satyamagarwal/blackjackgame/commits/89dec6a520b0b728f820f764ea9cfdba8524b025) ###

* I was very happy that I used recursion. :P But then I read more about it, and found out something odd. Here is the gist : even with tail call recursion (which is my case), you still need tail call optimization at compiler level, which JAVA doesn't have right now.
* So I switched to `do..while`.

### [Replace do..while with streams and fix business logic](https://bitbucket.org/satyamagarwal/blackjackgame/commits/eba26f2374cf2fe15338fd1d0a345dad91a6c01e) ###

* Using `do..while` solve my problem but introduced mutation in my code.
* I got stuck for quite some time looking for solution, when I couldn't find any, I started reading `vavr's API reference`.
* I found out `Stream.iterate` can solve my problem, which removed mutation.
* Also, while testing I found out, `dealer` was drawing an extra card if initial hands are less than 17 for dealer. Which sometimes, let the player win unfairly. Fixed that.

### [Make a multi player blackJack game](https://bitbucket.org/satyamagarwal/blackjackgame/commits/702e5847e9d89d3f5317d643c880ee336b0fe283) ###

* I was done with two player BlackJack game by last commit. 
* Out of curiosity, how its actually played, I watched final table for 2005 `World Series of BlackJack`.
* Turns out the game is actually scalable.
* So read some rules and ignored action which needs human intervention like, split, double down, soft hand, etc etc (I chose not to read more about them too.)
* I boiled down to following assumptions :
  ** Dealer serves card to all players, until each player has score a score minimum as 17.
  ** All the players who score more than 21, bust.
  ** All the players who score 21, natural blackjack
  ** All the players who score between 17 and 21, wait for dealers turn.
  ** Dealer serves him/herself until, its his/her score reached minimum 17.
  ** If Dealer scores more than 21, players with 21, or less than 21 wins.
  ** If Dealer scores 21, only players with 21, wins with Dealer.
  ** If Dealer score less than 21, players compare the scores, and winners are the players who either scored 21, or beat dealer's score, or tied with dealer's score.
* With these assumptions, at least the idea how to scale the game became pretty clear.
* While implementing, learnt few more APIs from stream.
* Reason to build `BlackJackMultiPlayer` class, is to I can reuse the methods, and keep the logic separate and readable.
* The most interesting thing was, how I was able to take one method at a time, and add multi player support to it. Tests helped a lot.

### [Add IntelliJ generated code coverage](https://bitbucket.org/satyamagarwal/blackjackgame/commits/de80c1de53c0d2535df75a03caecd5999362267b)###

* I am not proud of this commit. But now its there, so its there.

### In the end ###

* I hope you guys like the effort.
* I couldn't think of any more refactoring I could perform without loosing readability and ease of understand code. (Perhaps my less exp/knowledge)
* Don't forget to give feedback and suggestions to code.
