package game.blackjack.errors;

public enum ErrorCodes {
    EMPTY_PATH("1", "Path is either null or empty."),
    UNABLE_TO_READ_FILE("2", "File cannot be opened."),
    INVALID_CONTENT("3", "A file should contain 4 rows with comma separated cards, in the format as C2, C3..., C10, CJ, CQ, CK, CA. Next row D2, D3..., DA etc"),
    TOO_MANY_OR_FEW_DECK_FILES("4", "The input decks should only be 4"),
    INVALID_CONTENT_OR_UNREADABLE_FILE("4", "Either file cannot be opened or file is Corrupted. " +
            "A file should contain 4 rows with comma separated cards, in the format as C2, C3..., C10, CJ, CQ, CK, CA. Next row D2, D3..., DA etc");

    public final String code;
    public final String message;

    ErrorCodes(final String code, final String message) {
        this.code = code;
        this.message = message;
    }
}
