package game.blackjack.domain;

import game.blackjack.domain.dtos.CurrentRoundPosition;
import game.blackjack.domain.dtos.Deck;
import game.blackjack.domain.dtos.Player;
import game.blackjack.domain.dtos.PlayerType;
import game.blackjack.domain.dtos.SuitedCard;
import io.vavr.collection.List;
import io.vavr.collection.Stream;
import io.vavr.control.Option;

import java.util.stream.Collectors;

import static game.blackjack.domain.dtos.PlayerType.DEALER;
import static game.blackjack.domain.dtos.PlayerType.PLAYER;

public class BlackJack {

    private final DeckService deckService;

    public BlackJack() {
        this.deckService = new DeckService();
    }

    public List<Player> initGame(final List<String> paths, final List<String> players) {
        final Deck shuffledDeck = deckService.getShuffledDeckEitherFromFileOrGame(paths, 1);
        final List<SuitedCard> initialHands = getInitialHands(shuffledDeck, players.size());
        final Deck deckAfterInitialHands = getDeckAfterDealingHands(shuffledDeck, initialHands.size());

        final List<Player> playersPositionAfterInitialRound = dealInitialHandsAndGetPlayers(initialHands, players);

        final Option<Player> resultOfInitialHand = findResultOfInitialHands(playersPositionAfterInitialRound);

        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        if (resultOfInitialHand.isDefined()) {
            return List.of(resultOfInitialHand.get());
        } else {
            final Option<Player> resultAfterInitialHands = findResultAfterPreviousRound(playersPositionAfterInitialRound);
            if (resultAfterInitialHands.isDefined()) {
                return List.of(resultAfterInitialHands.get());
            } else {
                return List.of(drawCardsAndFindResult(currentRoundPosition).get());
            }
        }
    }

    public Option<Player> drawCardsAndFindResult(final CurrentRoundPosition currentRoundPosition) {

        final CurrentRoundPosition gameAfterPlayersTurn = drawCardsForPlayer(currentRoundPosition, PLAYER);
        final Option<Player> resultAfterPlayersTurn = findResultAfterPreviousRound(gameAfterPlayersTurn.getPlayers());
        if (resultAfterPlayersTurn.isDefined()) {
            return resultAfterPlayersTurn;
        } else {
            final CurrentRoundPosition finalGamePosition = drawCardsForDealer(gameAfterPlayersTurn, DEALER);
            return findResultAfterPreviousRound(finalGamePosition.getPlayers());
        }
    }

    public CurrentRoundPosition drawCardsForPlayer(final CurrentRoundPosition position,
                                                   final PlayerType playerType) {
        return Stream.iterate(position, p -> makePlayer(p, playerType))
                .filter(p -> p.getPlayers().find(sam -> sam.getPlayerType() == PLAYER).get().getScore() >= 17)
                .get();
    }

    public CurrentRoundPosition drawCardsForDealer(final CurrentRoundPosition position,
                                                   final PlayerType playerType) {
        final Player player = position.getPlayers().find(p -> p.getPlayerType() == PLAYER).get();

        return Stream.iterate(position, p -> makePlayer(p, playerType))
                .filter(p -> p.getPlayers()
                        .find(dealer -> dealer.getPlayerType() == DEALER).get().getScore() > player.getScore())
                .get();
    }

    public CurrentRoundPosition makePlayer(final CurrentRoundPosition position, final PlayerType playerType) {
        final Player player = position.getPlayers().find(p -> p.getPlayerType() == PLAYER).get();
        final Player dealer = position.getPlayers().find(p -> p.getPlayerType() == DEALER).get();
        final List<SuitedCard> currentDeck = position.getDeck().getSuitedCards();
        final Deck deckAfterDealingHand = new Deck(List.ofAll(currentDeck.pop()));

        if (playerType == PLAYER) {
            final List<Player> playersAfterDealingHand = List.of(new Player(player.getHands().push(currentDeck.get()),
                            player.getPlayerName(),
                            player.getPlayerType()),
                    dealer);
            return new CurrentRoundPosition(deckAfterDealingHand, playersAfterDealingHand);
        } else {
            final List<Player> playersAfterDealingHands = List.of(player,
                    new Player(dealer.getHands().push(currentDeck.get()),
                            dealer.getPlayerName(),
                            dealer.getPlayerType()));
            return new CurrentRoundPosition(deckAfterDealingHand, playersAfterDealingHands);
        }
    }

    public Option<Player> findResultAfterPreviousRound(final List<Player> playersPositionAfterInitialRound) {

        final Player player = playersPositionAfterInitialRound.find(p -> p.getPlayerType() == PLAYER).get();
        final Player dealer = playersPositionAfterInitialRound.find(d -> d.getPlayerType() == DEALER).get();

        if (player.getScore() > 21) {
            printFinalResults(dealer.getPlayerName(), playersPositionAfterInitialRound);
            return Option.of(dealer);
        } else if (dealer.getScore() > 21) {
            printFinalResults(player.getPlayerName(), playersPositionAfterInitialRound);
            return Option.of(player);
        } else if (player.getScore() >= 17 && dealer.getScore() > player.getScore()) {
            printFinalResults(dealer.getPlayerName(), playersPositionAfterInitialRound);
            return Option.of(dealer);
        } else if (dealer.getScore() >= 17 && dealer.getScore() < player.getScore()) {
            printFinalResults(player.getPlayerName(), playersPositionAfterInitialRound);
            return Option.of(player);
        } else {
            return Option.none();
        }
    }

    public List<SuitedCard> getInitialHands(final Deck deck, final int numberOfPlayersIncludingDealer) {
        return deck.getSuitedCards().slice(0, 2 * numberOfPlayersIncludingDealer);
    }

    public Deck getDeckAfterDealingHands(final Deck deck, final int cardsToRemove) {
        final List<SuitedCard> deckAfterRemovingCards = Stream.iterate(deck.getSuitedCards(), List::pop)
                .take(cardsToRemove + 1)
                .toList()
                .last();

        return new Deck(List.ofAll(deckAfterRemovingCards));
    }

    public List<Player> dealInitialHandsAndGetPlayers(final List<SuitedCard> initialHands,
                                                      final List<String> players) {

        final List<SuitedCard> firstHandForPlayers = initialHands.slice(0, initialHands.size() / 2);
        final List<SuitedCard> secondHandForPlayers = initialHands.slice(firstHandForPlayers.size(), initialHands.size());

        final List<Player> playersAfterFirstHand = firstHandForPlayers.zipWith(players, (card, playerName) -> {
            if (!playerName.equalsIgnoreCase(DEALER.name())) {
                return new Player(List.of(card), playerName, PLAYER);
            } else {
                return new Player(List.of(card), playerName, DEALER);
            }
        });

        return secondHandForPlayers
                .zipWith(playersAfterFirstHand, (card, previousPlayerHand) -> new Player(previousPlayerHand
                        .getHands()
                        .push(card),
                        previousPlayerHand.getPlayerName(),
                        previousPlayerHand.getPlayerType()));
    }

    public Option<Player> findResultOfInitialHands(final List<Player> players) {

        final Option<Player> player = players.find(p -> p.getPlayerType() == PLAYER);
        final Option<Player> dealer = players.find(d -> d.getPlayerType() == DEALER);

        if (player.get().getScore() == 21 && dealer.get().getScore() == 21) {
            printFinalResults(player.get().getPlayerName(), players);
            return player;
        } else if (player.get().getScore() == 22 && dealer.get().getScore() == 22) {
            printFinalResults(dealer.get().getPlayerName(), players);
            return dealer;
        } else if (player.get().getScore() == 21) {
            printFinalResults(player.get().getPlayerName(), players);
            return player;
        } else if (dealer.get().getScore() == 21) {
            printFinalResults(dealer.get().getPlayerName(), players);
            return dealer;
        } else {
            return Option.none();
        }
    }

    private void printFinalResults(final String winner, final List<Player> playersFinalScores) {
        System.out.println("--------------------------------------------");
        System.out.println(winner);
        playersFinalScores.forEach(player -> System.out.println(player.getPlayerName() + ": "
                + player.getHands().map(SuitedCard::toString).reverse().collect(Collectors.joining(", "))));
        System.out.println("--------------------------------------------");
    }
}
