package game.blackjack.domain;

import game.blackjack.domain.dtos.CurrentRoundPosition;
import game.blackjack.domain.dtos.Deck;
import game.blackjack.domain.dtos.Player;
import game.blackjack.domain.dtos.PlayerType;
import game.blackjack.domain.dtos.SuitedCard;
import io.vavr.collection.List;
import io.vavr.collection.Stream;

import java.util.stream.Collectors;

import static game.blackjack.domain.dtos.PlayerType.DEALER;
import static game.blackjack.domain.dtos.PlayerType.PLAYER;

public class BlackJackMultiPlayer {

    private final DeckService deckService;
    private final BlackJack blackJack;

    public BlackJackMultiPlayer() {
        this.deckService = new DeckService();
        this.blackJack = new BlackJack();
    }

    public List<Player> initializeAndPlayGame(final List<String> paths,
                                              final List<String> players) {
        final Deck shuffledDeck = deckService.getShuffledDeckEitherFromFileOrGame(paths, 4);
        final List<SuitedCard> initialHands = blackJack.getInitialHands(shuffledDeck, players.size());
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(shuffledDeck, initialHands.size());

        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands, players);

        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final CurrentRoundPosition positionAfterDealingCardsToPlayers = drawCardsForPlayers(currentRoundPosition,
                PLAYER);

        final CurrentRoundPosition finalPosition = drawCardsForDealer(positionAfterDealingCardsToPlayers);

        final List<Player> resultAfterFinalRound = findResultAfterFinalRound(finalPosition.getPlayers());

        printFinalResults(resultAfterFinalRound.map(Player::getPlayerName), finalPosition.getPlayers());

        return resultAfterFinalRound;

    }

    public CurrentRoundPosition drawCardsForPlayers(final CurrentRoundPosition position,
                                                    final PlayerType playerType) {
        return Stream.iterate(position, p -> addCardsToPlayer(p, playerType))
                .dropUntil(p -> {
                    final List<Integer> scoresLessThanSeventeen = p.getPlayers()
                            .filter(player -> player.getPlayerType() == playerType)
                            .map(Player::getScore)
                            .filter(score -> score < 17);
                    return scoresLessThanSeventeen.isEmpty();
                })
                .take(1)
                .toList()
                .last();
    }

    public CurrentRoundPosition addCardsToPlayer(final CurrentRoundPosition players, final PlayerType playerType) {
        final List<Player> playersThatWillNotReceiveHands = players.getPlayers()
                .filter(player -> player.getScore() >= 17 && player.getPlayerType() == playerType);
        final List<Player> playersToReceiveHands = players.getPlayers()
                .filter(player -> player.getScore() < 17 && player.getPlayerType() == playerType);
        final List<Player> dealer = players.getPlayers().filter(p -> p.getPlayerType() == DEALER);

        final List<SuitedCard> deck = players.getDeck().getSuitedCards();
        final List<SuitedCard> cardsToServe = deck.slice(0, playersToReceiveHands.size());

        final Deck deckAfterDealingHands = blackJack.getDeckAfterDealingHands(new Deck(deck),
                cardsToServe.size());

        final List<Player> playersPositions = playersToReceiveHands
                .zipWith(cardsToServe, (player, card) -> new Player(player.getHands().push(card),
                        player.getPlayerName(),
                        player.getPlayerType()));

        return new CurrentRoundPosition(deckAfterDealingHands,
                playersPositions
                        .pushAll(playersThatWillNotReceiveHands)
                        .pushAll(dealer)
                        .sortBy(Player::getPlayerName));
    }

    public CurrentRoundPosition drawCardsForDealer(final CurrentRoundPosition position) {
        return Stream.iterate(position, this::addCardsToDealer)
                .dropUntil(p -> {
                    final List<Integer> scoresLessThanSeventeen = p.getPlayers()
                            .filter(player -> player.getPlayerType() == DEALER)
                            .map(Player::getScore)
                            .filter(score -> score < 17);
                    return scoresLessThanSeventeen.isEmpty();
                })
                .take(1)
                .toList()
                .last();
    }

    public CurrentRoundPosition addCardsToDealer(final CurrentRoundPosition position) {
        final Player dealer = position.getPlayers().filter(p -> p.getPlayerType() == DEALER).get();
        final List<Player> players = position.getPlayers().filter(p -> p.getPlayerType() == PLAYER);

        if (dealer.getScore() < 17) {
            final List<SuitedCard> deck = position.getDeck().getSuitedCards();
            final SuitedCard cardToServe = deck.get();
            final Deck deckAfterDealingHands = new Deck(deck.pop());

            final Player dealersPosition = new Player(dealer.getHands().push(cardToServe),
                    dealer.getPlayerName(),
                    dealer.getPlayerType());

            return new CurrentRoundPosition(deckAfterDealingHands,
                    players.push(dealersPosition).sortBy(Player::getPlayerName));
        } else {
            return new CurrentRoundPosition(position.getDeck(), position.getPlayers().sortBy(Player::getPlayerName));
        }
    }

    public List<Player> findResultAfterFinalRound(final List<Player> playersPositionAfterInitialRound) {
        final List<Player> players = playersPositionAfterInitialRound
                .filter(p -> p.getPlayerType() == PLAYER && p.getScore() <= 21);
        final Player dealer = playersPositionAfterInitialRound.filter(d -> d.getPlayerType() == DEALER).get();

        if (players.isEmpty()) {
            return List.of(dealer);
        } else {
            if (dealer.getScore() > 21) {
                return players.sortBy(Player::getPlayerName);
            } else {
                final List<Player> playersWhoTiedWithDealer = players.filter(p -> p.getScore() == dealer.getScore());
                if (playersWhoTiedWithDealer.isEmpty()) {
                    final List<Player> playersWhoBeatDealer = players.filter(p -> p.getScore() > dealer.getScore());
                    if (!playersWhoBeatDealer.isEmpty()) {
                        return playersWhoBeatDealer.sortBy(Player::getPlayerName);
                    } else {
                        return List.of(dealer);
                    }
                } else {
                    return List.of(dealer)
                            .pushAll(players.filter(p -> p.getScore() >= dealer.getScore()))
                            .sortBy(Player::getPlayerName);
                }
            }
        }
    }

    private void printFinalResults(final List<String> winners, final List<Player> playersFinalScores) {
        System.out.println("--------------------------------------------");
        System.out.println(winners.collect(Collectors.joining(", ")));
        playersFinalScores.forEach(player -> System.out.println(player.getPlayerName() + ": "
                + player.getHands().map(SuitedCard::toString).reverse().collect(Collectors.joining(", "))));
        System.out.println("--------------------------------------------");
    }
}
