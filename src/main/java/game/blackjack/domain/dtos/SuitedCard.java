package game.blackjack.domain.dtos;

import java.util.Objects;

public class SuitedCard {

    private final Card card;
    private final Suite suite;

    public SuitedCard(final Card card, final Suite suite) {
        this.card = card;
        this.suite = suite;
    }

    public Card getCard() {
        return card;
    }

    public Suite getSuite() {
        return suite;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SuitedCard)) {
            return false;
        }
        final SuitedCard that = (SuitedCard) o;
        return card == that.card &&
                suite == that.suite;
    }

    @Override
    public int hashCode() {

        return Objects.hash(card, suite);
    }

    @Override
    public String toString() {
        return suite.getName() + card.getFace();
    }
}
