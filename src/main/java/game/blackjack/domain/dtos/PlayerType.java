package game.blackjack.domain.dtos;

public enum PlayerType {
    PLAYER, DEALER
}
