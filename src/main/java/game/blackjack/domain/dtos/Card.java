package game.blackjack.domain.dtos;

import io.vavr.collection.List;
import io.vavr.control.Option;

public enum Card {

    TWO("2", 2),
    THREE("3", 3),
    FOUR("4", 4),
    FIVE("5", 5),
    SIX("6", 6),
    SEVEN("7", 7),
    EIGHT("8", 8),
    NINE("9", 9),
    TEN("10", 10),
    JACK("J", 10),
    QUEEN("Q", 10),
    KING("K", 10),
    ACE("A", 11);

    private final String face;
    private final int value;

    Card(final String face, final int value) {
        this.face = face;
        this.value = value;
    }

    public static List<Card> getCards() {
        return List.of(Card.values());
    }

    public static Option<Card> getCardFromFace(final String face) {
        return getCards().find(card -> card.face.equalsIgnoreCase(face));
    }

    public static Option<Card> getCardFromValue(final int value) {
        return getCards().find(card -> card.value == value);
    }

    public String getFace() {
        return face;
    }

    public int getValue() {
        return value;
    }
}
