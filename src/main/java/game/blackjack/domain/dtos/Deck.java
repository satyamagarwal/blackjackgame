package game.blackjack.domain.dtos;

import io.vavr.collection.List;

public class Deck {

    private final List<SuitedCard> suitedCards;

    public Deck(final List<SuitedCard> suitedCards) {
        this.suitedCards = suitedCards;
    }

    public List<SuitedCard> getSuitedCards() {
        return suitedCards;
    }
}
