package game.blackjack.domain.dtos;


import io.vavr.collection.List;

public class CurrentRoundPosition {

    private final Deck deck;
    private final List<Player> players;

    public CurrentRoundPosition(final Deck deck, final List<Player> players) {
        this.deck = deck;
        this.players = players;
    }

    public Deck getDeck() {
        return deck;
    }

    public List<Player> getPlayers() {
        return players;
    }
}
