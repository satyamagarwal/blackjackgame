package game.blackjack.domain.dtos;


import io.vavr.collection.List;

public class Player {

    private final int score;
    private final List<SuitedCard> hands;
    private final String playerName;
    private final PlayerType playerType;

    public Player(final List<SuitedCard> hands,
                  final String playerName,
                  final PlayerType playerType) {
        this.score = hands.map(suitedCard -> suitedCard.getCard().getValue()).sum().intValue();
        this.hands = hands;
        this.playerName = playerName;
        this.playerType = playerType;
    }

    public int getScore() {
        return score;
    }

    public List<SuitedCard> getHands() {
        return hands;
    }

    public String getPlayerName() {
        return playerName;
    }

    public PlayerType getPlayerType() {
        return playerType;
    }
}
