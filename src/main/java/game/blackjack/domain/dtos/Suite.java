package game.blackjack.domain.dtos;

import io.vavr.collection.List;
import io.vavr.control.Option;

public enum Suite {

    CLUBS("C"),
    DIAMONDS("D"),
    HEARTS("H"),
    SPADES("S");

    private final String name;

    Suite(final String name) {
        this.name = name;
    }

    public static List<Suite> getSuites() {
        return List.of(Suite.values());
    }

    public static Option<Suite> getSuiteFromName(final String name) {
        return getSuites().find(suite -> suite.name.equalsIgnoreCase(name));
    }

    public String getName() {
        return name;
    }
}
