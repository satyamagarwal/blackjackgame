package game.blackjack.domain;

import game.blackjack.domain.dtos.Card;
import game.blackjack.domain.dtos.Deck;
import game.blackjack.domain.dtos.Suite;
import game.blackjack.domain.dtos.SuitedCard;
import game.blackjack.errors.Error;
import io.vavr.collection.List;
import io.vavr.control.Either;
import io.vavr.control.Option;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static game.blackjack.errors.ErrorCodes.INVALID_CONTENT;
import static game.blackjack.errors.ErrorCodes.TOO_MANY_OR_FEW_DECK_FILES;
import static game.blackjack.errors.ErrorCodes.UNABLE_TO_READ_FILE;
import static java.util.Objects.nonNull;

public class DeckService {

    public Deck initializeDeck() {
        final List<SuitedCard> suitedCards = Suite.getSuites()
                .flatMap(suite -> Card.getCards().map(card -> new SuitedCard(card, suite)));

        return new Deck(suitedCards);
    }

    public Deck getDecksFromGame(final int numberOfDecks) {
        final List<SuitedCard> suitedCards = initializeDeck().getSuitedCards();
        final int decksToAdd = numberOfDecks > 2 ? numberOfDecks : 1;

        final List<SuitedCard> cardsFromFourDecks = io.vavr.collection.Stream.continually(suitedCards)
                .take(decksToAdd)
                .flatMap(suitedCards1 -> suitedCards1)
                .toList();

        return new Deck(List.ofAll(cardsFromFourDecks));
    }

    public Either<Error, Deck> getDecksFromFile(final List<String> paths, final int numberOfDecks) {
        if (paths.filter(path -> nonNull(path) && !path.isEmpty()).size() != numberOfDecks) {
            return Either.left(new Error(TOO_MANY_OR_FEW_DECK_FILES.code, TOO_MANY_OR_FEW_DECK_FILES.message));
        }

        final Option<Deck> decks = paths.filter(path -> nonNull(path) && !path.isEmpty())
                .map(this::getDeckFromFile)
                .filter(Either::isRight)
                .map(Either::get)
                .reduceRightOption((deck1, deck2) -> new Deck(deck2.getSuitedCards().pushAll(deck1.getSuitedCards())));

        if (!decks.isDefined() || decks.get().getSuitedCards().size() != numberOfDecks * 52) {
            return Either.left(new Error(INVALID_CONTENT.code, INVALID_CONTENT.message));
        } else {
            return Either.right(decks.get());
        }
    }

    public Deck shuffleDeck(final Deck deck) {
        return new Deck(List.ofAll(deck.getSuitedCards().shuffle()));
    }

    public Deck shuffleDeck() {
        return new Deck(List.ofAll(initializeDeck().getSuitedCards().shuffle()));
    }

    public Deck getDeckAfterDealingHands(final Deck deck) {
        return new Deck(deck.getSuitedCards().pop());
    }

    public SuitedCard getDealtCard(final Deck deck) {
        return deck.getSuitedCards().get();
    }

    public Deck getShuffledDeckEitherFromFileOrGame(final List<String> paths, final int numberOfDecks) {
        return getDecksFromFile(paths, numberOfDecks)
                .peekLeft(error -> {
                    System.out.println(error);
                    System.out.println("------------------------------------------");
                    System.out.println("Initializing game with default shuffled deck");
                })
                .map(this::shuffleDeck)
                .getOrElse(shuffleDeck(getDecksFromGame(numberOfDecks)));
    }

    public Either<Error, Deck> getDeckFromFile(final String path) {
        try (Stream<String> stream = Files.lines(Paths.get(path))) {

            final List<SuitedCard> suitedCards = List.of(stream.collect(Collectors.joining(",")).split(","))
                    .map(String::trim)
                    .filter(word -> !word.isEmpty())
                    .map(this::validateWordSize)
                    .filter(Option::isDefined)
                    .map(Option::get)
                    .distinct();

            if (suitedCards.size() != 52) {
                return Either.left(new Error(INVALID_CONTENT.code, INVALID_CONTENT.message));
            } else {
                return Either.right(new Deck(suitedCards));
            }
        } catch (IOException | UncheckedIOException e) {
            return Either.left(new Error(UNABLE_TO_READ_FILE.code, UNABLE_TO_READ_FILE.message));
        }
    }

    private Option<SuitedCard> validateWordSize(final String word) {
        if (word.length() < 2 || word.length() > 3) {
            return Option.none();
        } else {
            return parseAndValidateWord(word);
        }
    }

    private Option<SuitedCard> parseAndValidateWord(final String word) {
        final Option<Suite> suite = Suite.getSuiteFromName(String.valueOf(word.charAt(0)));
        final Option<Card> card = Card.getCardFromFace(word.substring(1, word.length()));

        if (suite.isDefined() && card.isDefined()) {
            return Option.of(new SuitedCard(card.get(), suite.get()));
        } else {
            return Option.none();
        }
    }
}
