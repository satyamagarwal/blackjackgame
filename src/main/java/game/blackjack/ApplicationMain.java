package game.blackjack;

import game.blackjack.domain.BlackJack;
import game.blackjack.domain.BlackJackMultiPlayer;
import io.vavr.collection.List;

class ApplicationMain {

    public static void main(final String... args) {
        final BlackJack blackJack = new BlackJack();
        final BlackJackMultiPlayer blackJackMultiPlayer = new BlackJackMultiPlayer();
        final List<String> twoPlayerNames = List.of("Sam", "Dealer");
        final List<String> multiPlayerNames = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final List<String> deckFiles = List.of(args);

        if (deckFiles.size() == 1) {
            blackJack.initGame(deckFiles, twoPlayerNames);
        } else {
            blackJackMultiPlayer.initializeAndPlayGame(deckFiles, multiPlayerNames);
        }
    }
}
