package game.blackjack;

import game.blackjack.domain.dtos.Card;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CardTest {

    @Test
    public void should_get_a_list_of_cards() {
        final List<Card> cards = Card.getCards();

        assertThat(cards.size()).isEqualTo(13);
        assertThat(cards.get(0)).isEqualTo(Card.TWO);
        assertThat(cards.get(12)).isEqualTo(Card.ACE);
    }

    @Test
    public void should_get_a_card_from_face() {
        final Option<Card> card = Card.getCardFromFace("J");

        assertThat(card.isDefined()).isTrue();
        assertThat(card.get().getFace()).isEqualTo("J");
        assertThat(card.get().getValue()).isEqualTo(10);
        assertThat(card.get()).isEqualTo(Card.JACK);
    }

    @Test
    public void should_get_a_card_from_value() {
        final Option<Card> card = Card.getCardFromValue(10);

        assertThat(card.isDefined()).isTrue();
        assertThat(card.get().getFace()).isEqualTo("10");
        assertThat(card.get().getValue()).isEqualTo(10);
        assertThat(card.get()).isEqualTo(Card.TEN);
    }
}