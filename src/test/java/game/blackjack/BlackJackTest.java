package game.blackjack;

import game.blackjack.domain.BlackJack;
import game.blackjack.domain.DeckService;
import game.blackjack.domain.dtos.Card;
import game.blackjack.domain.dtos.CurrentRoundPosition;
import game.blackjack.domain.dtos.Deck;
import game.blackjack.domain.dtos.Player;
import game.blackjack.domain.dtos.Suite;
import game.blackjack.domain.dtos.SuitedCard;
import io.vavr.collection.List;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import org.junit.Test;

import static game.blackjack.domain.dtos.PlayerType.DEALER;
import static game.blackjack.domain.dtos.PlayerType.PLAYER;
import static org.assertj.core.api.Assertions.assertThat;

public class BlackJackTest {

    private final BlackJack blackJack = new BlackJack();
    private final DeckService deckService = new DeckService();

    @Test
    public void should_get_first_four_cards_of_shuffled_deck() {
        final Deck shuffledDeck = deckService.getShuffledDeckEitherFromFileOrGame(List.of(""), 1);
        final int numberOfPlayersIncludingDealer = 2;

        final List<SuitedCard> initialHands = blackJack.getInitialHands(shuffledDeck, numberOfPlayersIncludingDealer);

        assertThat(initialHands.size()).isEqualTo(4);
        assertThat(initialHands)
                .containsExactly(shuffledDeck.getSuitedCards().get(0),
                        shuffledDeck.getSuitedCards().get(1),
                        shuffledDeck.getSuitedCards().get(2),
                        shuffledDeck.getSuitedCards().get(3));
    }

    @Test
    public void should_get_deck_after_dealing_four_cards() {
        final Deck shuffledDeck = deckService.getShuffledDeckEitherFromFileOrGame(List.of(""), 1);
        final int numberOfPlayersIncludingDealer = 2;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(shuffledDeck, numberOfPlayersIncludingDealer);

        final Deck deckAfterDealingHands = blackJack.getDeckAfterDealingHands(shuffledDeck, initialHands.size());

        assertThat(deckAfterDealingHands.getSuitedCards().size()).isEqualTo(shuffledDeck.getSuitedCards().size() - 4);
        assertThat(deckAfterDealingHands.getSuitedCards().size()).isEqualTo(48);
    }

    @Test
    public void should_get_players_after_dealing_four_cards() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.ACE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.TWO, Suite.CLUBS);
        final SuitedCard thirdCard = new SuitedCard(Card.JACK, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.KING, Suite.DIAMONDS);
        final List<SuitedCard> initialHands = List.of(firstCard, secondCard, thirdCard, fourthCard);

        final List<Player> players = blackJack.dealInitialHandsAndGetPlayers(initialHands, playerNames);

        assertThat(players.size()).isEqualTo(2);

        assertThat(players.get(0).getPlayerName()).isEqualTo(playerNames.get(0));
        assertThat(players.get(0).getScore()).isEqualTo(21);
        assertThat(players.get(0).getHands()).containsExactly(thirdCard, firstCard);

        assertThat(players.get(1).getPlayerName()).isEqualTo(playerNames.get(1));
        assertThat(players.get(1).getScore()).isEqualTo(12);
        assertThat(players.get(1).getHands()).containsExactly(fourthCard, secondCard);

    }

    @Test
    public void player_should_win_initial_game_when_only_player_scores_blackjack() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.ACE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.TWO, Suite.CLUBS);
        final SuitedCard thirdCard = new SuitedCard(Card.JACK, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.KING, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultOfInitialHands(players);

        assertThat(result.isDefined()).isTrue();
        assertThat(result.get()).isEqualTo(players.get(0));
    }

    @Test
    public void player_should_win_initial_game_when_both_player_and_dealer_scores_blackjack() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.ACE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.ACE, Suite.HEARTS);
        final SuitedCard thirdCard = new SuitedCard(Card.JACK, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.KING, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultOfInitialHands(players);

        assertThat(result.isDefined()).isTrue();
        assertThat(result.get()).isEqualTo(players.get(0));
    }

    @Test
    public void dealer_should_win_initial_game_when_both_player_and_dealer_scores_twenty_two() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.ACE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.ACE, Suite.HEARTS);
        final SuitedCard thirdCard = new SuitedCard(Card.ACE, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.ACE, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultOfInitialHands(players);

        assertThat(result.isDefined()).isTrue();
        assertThat(result.get()).isEqualTo(players.get(1));
    }

    @Test
    public void no_one_should_win_initial_game() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.TWO, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.ACE, Suite.HEARTS);
        final SuitedCard thirdCard = new SuitedCard(Card.THREE, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.ACE, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultOfInitialHands(players);

        assertThat(result.isDefined()).isFalse();
    }

    @Test
    public void dealer_should_win_game_because_player_scored_more_than_twenty_one() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.ACE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.ACE, Suite.HEARTS);
        final SuitedCard thirdCard = new SuitedCard(Card.ACE, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.SEVEN, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultAfterPreviousRound(players);

        assertThat(result.isDefined()).isTrue();
        assertThat(result.get()).isEqualTo(players.get(1));
    }

    @Test
    public void player_should_win_game_because_dealer_scored_more_than_twenty_one() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.NINE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.ACE, Suite.HEARTS);
        final SuitedCard thirdCard = new SuitedCard(Card.EIGHT, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.ACE, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultAfterPreviousRound(players);

        assertThat(result.isDefined()).isTrue();
        assertThat(result.get()).isEqualTo(players.get(0));
    }

    @Test
    public void dealer_should_win_game_because_player_scored_less_than_dealer() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.NINE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.ACE, Suite.HEARTS);
        final SuitedCard thirdCard = new SuitedCard(Card.EIGHT, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.NINE, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultAfterPreviousRound(players);

        assertThat(result.isDefined()).isTrue();
        assertThat(result.get()).isEqualTo(players.get(1));
    }

    @Test
    public void dealer_should_not_win_game_because_dealer_scored_less_than_player() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.NINE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.TWO, Suite.HEARTS);
        final SuitedCard thirdCard = new SuitedCard(Card.EIGHT, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.THREE, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultAfterPreviousRound(players);

        assertThat(result.isDefined()).isFalse();
    }

    @Test
    public void nobody_should_win_game_if_player_scored_less_than_17_after_initial_hand() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final SuitedCard firstCard = new SuitedCard(Card.NINE, Suite.CLUBS);
        final SuitedCard secondCard = new SuitedCard(Card.TWO, Suite.HEARTS);
        final SuitedCard thirdCard = new SuitedCard(Card.SIX, Suite.SPADES);
        final SuitedCard fourthCard = new SuitedCard(Card.THREE, Suite.DIAMONDS);
        final List<Player> players = List.of(new Player(List.of(firstCard, thirdCard), playerNames.get(0), PLAYER),
                new Player(List.of(secondCard, fourthCard), playerNames.get(1), DEALER));

        final Option<Player> result = blackJack.findResultAfterPreviousRound(players);

        assertThat(result.isDefined()).isFalse();
    }

    @Test
    public void should_draw_cards_for_player_until_score_is_higher_than_17() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final Deck orderedDeck = deckService.initializeDeck();
        final int numberOfPlayersIncludingDealer = 2;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                playerNames);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final CurrentRoundPosition positionAfterPlayerHands = blackJack.drawCardsForPlayer(currentRoundPosition, PLAYER);

        assertThat(positionAfterPlayerHands.getPlayers().get(0).getScore()).isGreaterThanOrEqualTo(17);
        assertThat(positionAfterPlayerHands.getDeck().getSuitedCards())
                .doesNotContainAnyElementsOf(positionAfterPlayerHands.getPlayers().get(0).getHands());
    }

    @Test
    public void should_draw_cards_for_dealer_until_score_is_higher_than_player() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final Deck orderedDeck = deckService.initializeDeck();
        final int numberOfPlayersIncludingDealer = 2;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                playerNames);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);
        final CurrentRoundPosition positionAfterPlayerHands = blackJack.drawCardsForPlayer(currentRoundPosition, PLAYER);

        final CurrentRoundPosition gameAfterDealersTurn = blackJack.drawCardsForDealer(positionAfterPlayerHands, DEALER);

        assertThat(gameAfterDealersTurn.getPlayers().get(1).getScore())
                .isGreaterThan(currentRoundPosition.getPlayers().get(0).getScore());
        assertThat(currentRoundPosition.getDeck().getSuitedCards())
                .doesNotContainAnyElementsOf(currentRoundPosition.getPlayers().get(0).getHands());
        assertThat(gameAfterDealersTurn.getDeck().getSuitedCards())
                .doesNotContainAnyElementsOf(gameAfterDealersTurn.getPlayers().get(1).getHands());
        assertThat(gameAfterDealersTurn.getDeck().getSuitedCards())
                .isNotEqualTo(currentRoundPosition.getDeck());
        assertThat(gameAfterDealersTurn.getDeck().getSuitedCards())
                .isNotEqualTo(deckAfterInitialHands.getSuitedCards());
    }

    @Test
    public void should_be_able_to_return_game_position_after_dealing_one_hand_to_player() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final Deck orderedDeck = deckService.initializeDeck();
        final int numberOfPlayersIncludingDealer = 2;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                playerNames);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final CurrentRoundPosition gameAfterPlayerTurn = blackJack.makePlayer(currentRoundPosition, PLAYER);

        assertThat(gameAfterPlayerTurn.getPlayers().get(0)).isNotEqualTo(currentRoundPosition.getPlayers().get(0));
        assertThat(gameAfterPlayerTurn.getPlayers().get(1)).isEqualTo(currentRoundPosition.getPlayers().get(1));
        assertThat(gameAfterPlayerTurn.getPlayers().get(0).getScore())
                .isGreaterThan(currentRoundPosition.getPlayers().get(0).getScore());
        assertThat(gameAfterPlayerTurn.getDeck().getSuitedCards())
                .doesNotContain(currentRoundPosition.getDeck().getSuitedCards().get(0));
    }

    @Test
    public void should_be_able_to_return_game_position_after_dealing_one_hand_to_dealer() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final Deck orderedDeck = deckService.initializeDeck();
        final int numberOfPlayersIncludingDealer = 2;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                playerNames);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final CurrentRoundPosition gameAfterPlayerTurn = blackJack.makePlayer(currentRoundPosition, DEALER);

        assertThat(gameAfterPlayerTurn.getPlayers().get(1)).isNotEqualTo(currentRoundPosition.getPlayers().get(1));
        assertThat(gameAfterPlayerTurn.getPlayers().get(0)).isEqualTo(currentRoundPosition.getPlayers().get(0));
        assertThat(gameAfterPlayerTurn.getPlayers().get(1).getScore())
                .isGreaterThan(currentRoundPosition.getPlayers().get(1).getScore());
        assertThat(gameAfterPlayerTurn.getDeck().getSuitedCards())
                .doesNotContain(currentRoundPosition.getDeck().getSuitedCards().get(0));
    }

    @Test
    public void player_should_win() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final Deck orderedDeck = deckService.initializeDeck();
        final int numberOfPlayersIncludingDealer = 2;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                playerNames);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final Option<Player> player = blackJack.drawCardsAndFindResult(currentRoundPosition);

        assertThat(player.isDefined()).isTrue();
        assertThat(player.get().getPlayerType()).isEqualTo(PLAYER);
    }

    @Test
    public void dealer_should_win() {
        final List<String> playerNames = List.of("Sam", "Dealer");
        final Deck orderedDeck = deckService.getDeckFromFile("src/test/resources/DeckForDealerToWin.txt").get();
        final int numberOfPlayersIncludingDealer = 2;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                playerNames);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final Option<Player> player = blackJack.drawCardsAndFindResult(currentRoundPosition);

        assertThat(player.isDefined()).isTrue();
        assertThat(player.get().getPlayerType()).isEqualTo(DEALER);
    }

    @Test
    public void find_a_winner() {
        final List<String> paths = List.of("src/test/resources/Deck.txt");
        final List<String> players = List.of("Sam", "Dealer");
        Stream.rangeClosed(1, 1000)
                .forEach(number -> {
                    final List<Player> player = blackJack.initGame(paths, players);

                    assertThat(player).isNotEmpty();
                });
    }

    @Test
    public void should_get_players_after_dealing_initial_hands_to_players() {
        final List<String> playerNames = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final List<SuitedCard> initialHands = List.of(new SuitedCard(Card.ACE, Suite.CLUBS), new SuitedCard(Card.KING, Suite.CLUBS),
                new SuitedCard(Card.QUEEN, Suite.CLUBS), new SuitedCard(Card.JACK, Suite.CLUBS),
                new SuitedCard(Card.TEN, Suite.CLUBS), new SuitedCard(Card.NINE, Suite.CLUBS),
                new SuitedCard(Card.EIGHT, Suite.CLUBS), new SuitedCard(Card.SEVEN, Suite.CLUBS),
                new SuitedCard(Card.SIX, Suite.CLUBS), new SuitedCard(Card.FIVE, Suite.CLUBS),
                new SuitedCard(Card.FOUR, Suite.CLUBS), new SuitedCard(Card.THREE, Suite.CLUBS));

        final List<Player> players = blackJack.dealInitialHandsAndGetPlayers(initialHands, playerNames);

        assertThat(players.size()).isEqualTo(6);
        assertThat(players.get(0).getScore()).isEqualTo(19);
        assertThat(players.get(1).getScore()).isEqualTo(17);
        assertThat(players.get(2).getScore()).isEqualTo(16);
        assertThat(players.get(3).getScore()).isEqualTo(15);
        assertThat(players.get(4).getScore()).isEqualTo(14);
        assertThat(players.get(5).getScore()).isEqualTo(12);
    }
}