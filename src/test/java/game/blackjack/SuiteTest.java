package game.blackjack;

import game.blackjack.domain.dtos.Suite;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SuiteTest {

    @Test
    public void should_get_a_list_of_suites() {
        final List<Suite> suites = Suite.getSuites();

        assertThat(suites.size()).isEqualTo(4);
        assertThat(suites.get(0)).isEqualTo(Suite.CLUBS);
        assertThat(suites.get(3)).isEqualTo(Suite.SPADES);
    }

    @Test
    public void should_get_a_suite_by_name() {
        final Option<Suite> suite = Suite.getSuiteFromName("D");

        assertThat(suite.isDefined()).isTrue();
        assertThat(suite.get().getName()).isEqualTo("D");
        assertThat(suite.get()).isEqualTo(Suite.DIAMONDS);
    }

}