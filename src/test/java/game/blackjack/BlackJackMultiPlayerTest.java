package game.blackjack;

import game.blackjack.domain.BlackJack;
import game.blackjack.domain.BlackJackMultiPlayer;
import game.blackjack.domain.DeckService;
import game.blackjack.domain.dtos.Card;
import game.blackjack.domain.dtos.CurrentRoundPosition;
import game.blackjack.domain.dtos.Deck;
import game.blackjack.domain.dtos.Player;
import game.blackjack.domain.dtos.Suite;
import game.blackjack.domain.dtos.SuitedCard;
import io.vavr.collection.List;
import io.vavr.collection.Stream;
import org.junit.Test;

import static game.blackjack.domain.dtos.Card.ACE;
import static game.blackjack.domain.dtos.Card.EIGHT;
import static game.blackjack.domain.dtos.Card.FIVE;
import static game.blackjack.domain.dtos.Card.FOUR;
import static game.blackjack.domain.dtos.Card.JACK;
import static game.blackjack.domain.dtos.Card.KING;
import static game.blackjack.domain.dtos.Card.NINE;
import static game.blackjack.domain.dtos.Card.QUEEN;
import static game.blackjack.domain.dtos.Card.SEVEN;
import static game.blackjack.domain.dtos.Card.SIX;
import static game.blackjack.domain.dtos.Card.TEN;
import static game.blackjack.domain.dtos.Card.THREE;
import static game.blackjack.domain.dtos.Card.TWO;
import static game.blackjack.domain.dtos.PlayerType.DEALER;
import static game.blackjack.domain.dtos.PlayerType.PLAYER;
import static game.blackjack.domain.dtos.Suite.CLUBS;
import static game.blackjack.domain.dtos.Suite.DIAMONDS;
import static org.assertj.core.api.Assertions.assertThat;

public class BlackJackMultiPlayerTest {

    private final DeckService deckService = new DeckService();
    private final BlackJack blackJack = new BlackJack();
    private final BlackJackMultiPlayer blackJackMultiPlayer = new BlackJackMultiPlayer();

    @Test
    public void should_be_able_to_add_cards_for_all_players() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final int numberOfPlayersIncludingDealer = 6;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final CurrentRoundPosition finalPositions = blackJackMultiPlayer.addCardsToPlayer(currentRoundPosition, PLAYER);

        assertThat(finalPositions.getPlayers().get(0).getScore()).isEqualTo(17);
        assertThat(finalPositions.getPlayers().get(1).getScore()).isEqualTo(21);
        assertThat(finalPositions.getPlayers().get(2).getScore()).isEqualTo(14);
        assertThat(finalPositions.getPlayers().get(3).getScore()).isEqualTo(17);
        assertThat(finalPositions.getPlayers().get(4).getScore()).isEqualTo(19);
        assertThat(finalPositions.getPlayers().get(5).getScore()).isEqualTo(21);

        assertThat(finalPositions.getDeck().getSuitedCards().size()).isEqualTo(191);

        assertThat(finalPositions.getPlayers().get(0).getHands()).isEqualTo(List.of(hand(KING, CLUBS), hand(SEVEN, CLUBS)));
        assertThat(finalPositions.getPlayers().get(1).getHands()).isEqualTo(List.of(hand(ACE, CLUBS), hand(EIGHT, CLUBS), hand(TWO, CLUBS)));
        assertThat(finalPositions.getPlayers().get(2).getHands()).isEqualTo(List.of(hand(TWO, DIAMONDS), hand(NINE, CLUBS), hand(THREE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(3).getHands()).isEqualTo(List.of(hand(THREE, DIAMONDS), hand(TEN, CLUBS), hand(FOUR, CLUBS)));
        assertThat(finalPositions.getPlayers().get(4).getHands()).isEqualTo(List.of(hand(FOUR, DIAMONDS), hand(JACK, CLUBS), hand(FIVE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(5).getHands()).isEqualTo(List.of(hand(FIVE, DIAMONDS), hand(QUEEN, CLUBS), hand(SIX, CLUBS)));
    }

    @Test
    public void should_not_be_able_to_add_cards_for_any_players() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final int numberOfPlayersIncludingDealer = 6;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final List<Player> newPlayers = currentRoundPosition.getPlayers()
                .map(p -> new Player(p.getHands().push(hand(TEN, CLUBS)), p.getPlayerName(), p.getPlayerType()));

        final CurrentRoundPosition dummyPosition = new CurrentRoundPosition(currentRoundPosition.getDeck(),
                newPlayers.sortBy(Player::getPlayerName));

        final CurrentRoundPosition finalPositions = blackJackMultiPlayer.addCardsToPlayer(dummyPosition, PLAYER);

        assertThat(finalPositions.getPlayers().get(0).getScore()).isEqualTo(27);
        assertThat(finalPositions.getPlayers().get(1).getScore()).isEqualTo(20);
        assertThat(finalPositions.getPlayers().get(2).getScore()).isEqualTo(22);
        assertThat(finalPositions.getPlayers().get(3).getScore()).isEqualTo(24);
        assertThat(finalPositions.getPlayers().get(4).getScore()).isEqualTo(25);
        assertThat(finalPositions.getPlayers().get(5).getScore()).isEqualTo(26);

        assertThat(finalPositions.getDeck().getSuitedCards().size()).isEqualTo(196);

        assertThat(finalPositions.getPlayers().get(0).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(KING, CLUBS), hand(SEVEN, CLUBS)));
        assertThat(finalPositions.getPlayers().get(1).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(EIGHT, CLUBS), hand(TWO, CLUBS)));
        assertThat(finalPositions.getPlayers().get(2).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(NINE, CLUBS), hand(THREE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(3).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(TEN, CLUBS), hand(FOUR, CLUBS)));
        assertThat(finalPositions.getPlayers().get(4).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(JACK, CLUBS), hand(FIVE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(5).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(QUEEN, CLUBS), hand(SIX, CLUBS)));
    }

    @Test
    public void should_add_cards_for_dealer() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final int numberOfPlayersIncludingDealer = 6;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);
        final List<Player> newPlayers = currentRoundPosition.getPlayers()
                .removeFirst(p -> p.getPlayerType() == DEALER);
        final Player dummyDealer = new Player(List.of(hand(KING, CLUBS)), "Dealer", DEALER);

        final CurrentRoundPosition dummyPosition = new CurrentRoundPosition(currentRoundPosition.getDeck(),
                newPlayers.push(dummyDealer).sortBy(Player::getPlayerName));

        final CurrentRoundPosition finalPositions = blackJackMultiPlayer.addCardsToDealer(dummyPosition);


        assertThat(finalPositions.getPlayers().get(0).getScore()).isEqualTo(21);
        assertThat(finalPositions.getPlayers().get(1).getScore()).isEqualTo(10);
        assertThat(finalPositions.getPlayers().get(2).getScore()).isEqualTo(12);
        assertThat(finalPositions.getPlayers().get(3).getScore()).isEqualTo(14);
        assertThat(finalPositions.getPlayers().get(4).getScore()).isEqualTo(15);
        assertThat(finalPositions.getPlayers().get(5).getScore()).isEqualTo(16);

        assertThat(finalPositions.getDeck().getSuitedCards().size()).isEqualTo(195);

        assertThat(finalPositions.getPlayers().get(0).getHands()).isEqualTo(List.of(hand(ACE, CLUBS), hand(KING, CLUBS)));
        assertThat(finalPositions.getPlayers().get(1).getHands()).isEqualTo(List.of(hand(EIGHT, CLUBS), hand(TWO, CLUBS)));
        assertThat(finalPositions.getPlayers().get(2).getHands()).isEqualTo(List.of(hand(NINE, CLUBS), hand(THREE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(3).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(FOUR, CLUBS)));
        assertThat(finalPositions.getPlayers().get(4).getHands()).isEqualTo(List.of(hand(JACK, CLUBS), hand(FIVE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(5).getHands()).isEqualTo(List.of(hand(QUEEN, CLUBS), hand(SIX, CLUBS)));
    }

    @Test
    public void should_not_add_cards_for_dealer() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final int numberOfPlayersIncludingDealer = 6;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final CurrentRoundPosition finalPositions = blackJackMultiPlayer.addCardsToDealer(currentRoundPosition);

        assertThat(finalPositions.getPlayers().get(0).getScore()).isEqualTo(17);
        assertThat(finalPositions.getPlayers().get(1).getScore()).isEqualTo(10);
        assertThat(finalPositions.getPlayers().get(2).getScore()).isEqualTo(12);
        assertThat(finalPositions.getPlayers().get(3).getScore()).isEqualTo(14);
        assertThat(finalPositions.getPlayers().get(4).getScore()).isEqualTo(15);
        assertThat(finalPositions.getPlayers().get(5).getScore()).isEqualTo(16);

        assertThat(finalPositions.getDeck().getSuitedCards().size()).isEqualTo(196);

        assertThat(finalPositions.getPlayers().get(0).getHands()).isEqualTo(List.of(hand(KING, CLUBS), hand(SEVEN, CLUBS)));
        assertThat(finalPositions.getPlayers().get(1).getHands()).isEqualTo(List.of(hand(EIGHT, CLUBS), hand(TWO, CLUBS)));
        assertThat(finalPositions.getPlayers().get(2).getHands()).isEqualTo(List.of(hand(NINE, CLUBS), hand(THREE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(3).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(FOUR, CLUBS)));
        assertThat(finalPositions.getPlayers().get(4).getHands()).isEqualTo(List.of(hand(JACK, CLUBS), hand(FIVE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(5).getHands()).isEqualTo(List.of(hand(QUEEN, CLUBS), hand(SIX, CLUBS)));
    }

    @Test
    public void should_add_cards_for_player_until_every_player_reached_min_seventeen() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final Deck newOrderedDeck = new Deck(orderedDeck.getSuitedCards().sortBy(SuitedCard::toString));
        final int numberOfPlayersIncludingDealer = 6;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(newOrderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(newOrderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final CurrentRoundPosition finalPositions = blackJackMultiPlayer.drawCardsForPlayers(currentRoundPosition,
                PLAYER
        );

        assertThat(finalPositions.getPlayers().get(0).getScore()).isEqualTo(5);
        assertThat(finalPositions.getPlayers().get(1).getScore()).isEqualTo(21);
        assertThat(finalPositions.getPlayers().get(2).getScore()).isEqualTo(21);
        assertThat(finalPositions.getPlayers().get(3).getScore()).isEqualTo(17);
        assertThat(finalPositions.getPlayers().get(4).getScore()).isEqualTo(17);
        assertThat(finalPositions.getPlayers().get(5).getScore()).isEqualTo(21);

        assertThat(finalPositions.getDeck().getSuitedCards().size()).isEqualTo(187);
    }

    @Test
    public void should_not_be_able_to_add_cards_for_any_players_from_drawCardsForPlayers() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final int numberOfPlayersIncludingDealer = 6;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final List<Player> newPlayers = currentRoundPosition.getPlayers()
                .map(p -> new Player(p.getHands().push(hand(TEN, CLUBS)), p.getPlayerName(), p.getPlayerType()));

        final CurrentRoundPosition dummyPosition = new CurrentRoundPosition(currentRoundPosition.getDeck(),
                newPlayers.sortBy(Player::getPlayerName));

        final CurrentRoundPosition finalPositions = blackJackMultiPlayer.drawCardsForPlayers(dummyPosition, PLAYER);

        assertThat(finalPositions.getPlayers().get(0).getScore()).isEqualTo(27);
        assertThat(finalPositions.getPlayers().get(1).getScore()).isEqualTo(20);
        assertThat(finalPositions.getPlayers().get(2).getScore()).isEqualTo(22);
        assertThat(finalPositions.getPlayers().get(3).getScore()).isEqualTo(24);
        assertThat(finalPositions.getPlayers().get(4).getScore()).isEqualTo(25);
        assertThat(finalPositions.getPlayers().get(5).getScore()).isEqualTo(26);

        assertThat(finalPositions.getDeck().getSuitedCards().size()).isEqualTo(196);

        assertThat(finalPositions.getPlayers().get(0).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(KING, CLUBS), hand(SEVEN, CLUBS)));
        assertThat(finalPositions.getPlayers().get(1).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(EIGHT, CLUBS), hand(TWO, CLUBS)));
        assertThat(finalPositions.getPlayers().get(2).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(NINE, CLUBS), hand(THREE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(3).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(TEN, CLUBS), hand(FOUR, CLUBS)));
        assertThat(finalPositions.getPlayers().get(4).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(JACK, CLUBS), hand(FIVE, CLUBS)));
        assertThat(finalPositions.getPlayers().get(5).getHands()).isEqualTo(List.of(hand(TEN, CLUBS), hand(QUEEN, CLUBS), hand(SIX, CLUBS)));
    }

    @Test
    public void should_add_cards_for_dealer_until_dealer_has_reached_min_seventeen() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final Deck newOrderedDeck = new Deck(orderedDeck.getSuitedCards().sortBy(SuitedCard::toString));
        final int numberOfPlayersIncludingDealer = 6;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(newOrderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(newOrderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final CurrentRoundPosition finalPositions = blackJackMultiPlayer.drawCardsForDealer(currentRoundPosition);

        assertThat(finalPositions.getPlayers().get(0).getScore()).isEqualTo(17);
        assertThat(finalPositions.getPlayers().get(1).getScore()).isEqualTo(12);
        assertThat(finalPositions.getPlayers().get(2).getScore()).isEqualTo(12);
        assertThat(finalPositions.getPlayers().get(3).getScore()).isEqualTo(13);
        assertThat(finalPositions.getPlayers().get(4).getScore()).isEqualTo(13);
        assertThat(finalPositions.getPlayers().get(5).getScore()).isEqualTo(5);

        assertThat(finalPositions.getDeck().getSuitedCards().size()).isEqualTo(193);
    }

    @Test
    public void should_not_add_cards_for_dealer_from_drawCardsForDealer() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final int numberOfPlayersIncludingDealer = 6;
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, numberOfPlayersIncludingDealer);
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands,
                players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);

        final Player dummyDealer = new Player(List.of(hand(KING, CLUBS), hand(KING, CLUBS)), "Dealer", DEALER);

        final List<Player> newPosition = playersPositionAfterInitialRound.filter(p -> p.getPlayerType() == PLAYER)
                .push(dummyDealer);

        final CurrentRoundPosition dummyPosition = new CurrentRoundPosition(currentRoundPosition.getDeck(),
                newPosition);

        final CurrentRoundPosition finalPositions = blackJackMultiPlayer.drawCardsForDealer(dummyPosition);

        assertThat(finalPositions.getPlayers().get(0).getScore()).isEqualTo(20);
        assertThat(finalPositions.getPlayers().get(1).getScore()).isEqualTo(10);
        assertThat(finalPositions.getPlayers().get(2).getScore()).isEqualTo(12);
        assertThat(finalPositions.getPlayers().get(3).getScore()).isEqualTo(14);
        assertThat(finalPositions.getPlayers().get(4).getScore()).isEqualTo(15);
        assertThat(finalPositions.getPlayers().get(5).getScore()).isEqualTo(16);

        assertThat(finalPositions.getDeck().getSuitedCards().size()).isEqualTo(196);
    }

    @Test
    public void all_players_and_dealer_should_win() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, players.size());
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands, players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);
        final CurrentRoundPosition positionAfterDealingCardsToPlayers = blackJackMultiPlayer.drawCardsForPlayers(currentRoundPosition,
                PLAYER);
        final CurrentRoundPosition finalPosition = blackJackMultiPlayer.drawCardsForDealer(positionAfterDealingCardsToPlayers);

        final List<Player> result = blackJackMultiPlayer.findResultAfterFinalRound(finalPosition.getPlayers());

        assertThat(finalPosition.getDeck().getSuitedCards().size()).isEqualTo(190);

        assertThat(result.get(0).getScore()).isEqualTo(17);
        assertThat(result.get(1).getScore()).isEqualTo(21);
        assertThat(result.get(2).getScore()).isEqualTo(20);
        assertThat(result.get(3).getScore()).isEqualTo(17);
        assertThat(result.get(4).getScore()).isEqualTo(19);
        assertThat(result.get(5).getScore()).isEqualTo(21);

        assertThat(result.get(0).getHands()).isEqualTo(List.of(hand(KING, CLUBS), hand(SEVEN, CLUBS)));
        assertThat(result.get(1).getHands()).isEqualTo(List.of(hand(ACE, CLUBS), hand(EIGHT, CLUBS), hand(TWO, CLUBS)));
        assertThat(result.get(2).getHands()).isEqualTo(List.of(hand(SIX, DIAMONDS), hand(TWO, DIAMONDS), hand(NINE, CLUBS), hand(THREE, CLUBS)));
        assertThat(result.get(3).getHands()).isEqualTo(List.of(hand(THREE, DIAMONDS), hand(TEN, CLUBS), hand(FOUR, CLUBS)));
        assertThat(result.get(4).getHands()).isEqualTo(List.of(hand(FOUR, DIAMONDS), hand(JACK, CLUBS), hand(FIVE, CLUBS)));
        assertThat(result.get(5).getHands()).isEqualTo(List.of(hand(FIVE, DIAMONDS), hand(QUEEN, CLUBS), hand(SIX, CLUBS)));
    }

    @Test
    public void only_dealer_should_win() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, players.size());
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands, players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);
        final CurrentRoundPosition positionAfterDealingCardsToPlayers = blackJackMultiPlayer.drawCardsForPlayers(currentRoundPosition,
                PLAYER);
        final CurrentRoundPosition finalPosition = blackJackMultiPlayer.drawCardsForDealer(positionAfterDealingCardsToPlayers);
        final List<Player> newPlayers = finalPosition.getPlayers()
                .filter(p -> p.getPlayerType() == PLAYER)
                .map(p -> new Player(p.getHands().push(hand(TEN, CLUBS)), p.getPlayerName(), p.getPlayerType()));
        final List<Player> dealer = finalPosition.getPlayers().filter(p -> p.getPlayerType() == DEALER);
        final CurrentRoundPosition dummyPosition = new CurrentRoundPosition(finalPosition.getDeck(),
                newPlayers.pushAll(dealer));

        final List<Player> result = blackJackMultiPlayer.findResultAfterFinalRound(dummyPosition.getPlayers());

        assertThat(finalPosition.getDeck().getSuitedCards().size()).isEqualTo(190);

        assertThat(result.get(0).getScore()).isEqualTo(17);

        assertThat(result.get(0).getHands()).isEqualTo(List.of(hand(KING, CLUBS), hand(SEVEN, CLUBS)));
    }

    @Test
    public void only_players_should_win() {
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        final Deck orderedDeck = deckService.getDecksFromGame(4);
        final List<SuitedCard> initialHands = blackJack.getInitialHands(orderedDeck, players.size());
        final Deck deckAfterInitialHands = blackJack.getDeckAfterDealingHands(orderedDeck, initialHands.size());
        final List<Player> playersPositionAfterInitialRound = blackJack.dealInitialHandsAndGetPlayers(initialHands, players);
        final CurrentRoundPosition currentRoundPosition = new CurrentRoundPosition(deckAfterInitialHands,
                playersPositionAfterInitialRound);
        final CurrentRoundPosition positionAfterDealingCardsToPlayers = blackJackMultiPlayer.drawCardsForPlayers(currentRoundPosition,
                PLAYER);
        final CurrentRoundPosition finalPosition = blackJackMultiPlayer.drawCardsForDealer(positionAfterDealingCardsToPlayers);

        final Player dealer = finalPosition.getPlayers().filter(p -> p.getPlayerType() == DEALER).get();
        final Player dummyDealer = new Player(dealer.getHands().push(hand(KING, CLUBS)),
                dealer.getPlayerName(),
                dealer.getPlayerType());
        final Player player = finalPosition.getPlayers().filter(p -> p.getPlayerName().equals("Player3")).get(0);
        final Player dummyPlayer = new Player(player.getHands().push(hand(KING, CLUBS)),
                player.getPlayerName(),
                player.getPlayerType());
        final List<Player> finalPlayersPositions = finalPosition.getPlayers()
                .remove(dealer)
                .remove(player)
                .push(dummyDealer)
                .push(dummyPlayer);

        final List<Player> result = blackJackMultiPlayer.findResultAfterFinalRound(finalPlayersPositions);

        assertThat(finalPosition.getDeck().getSuitedCards().size()).isEqualTo(190);

        assertThat(result.get(0).getScore()).isEqualTo(21);
        assertThat(result.get(1).getScore()).isEqualTo(20);
        assertThat(result.get(2).getScore()).isEqualTo(19);
        assertThat(result.get(3).getScore()).isEqualTo(21);

        assertThat(result.get(0).getHands()).isEqualTo(List.of(hand(ACE, CLUBS), hand(EIGHT, CLUBS), hand(TWO, CLUBS)));
        assertThat(result.get(1).getHands()).isEqualTo(List.of(hand(SIX, DIAMONDS), hand(TWO, DIAMONDS), hand(NINE, CLUBS), hand(THREE, CLUBS)));
        assertThat(result.get(2).getHands()).isEqualTo(List.of(hand(FOUR, DIAMONDS), hand(JACK, CLUBS), hand(FIVE, CLUBS)));
        assertThat(result.get(3).getHands()).isEqualTo(List.of(hand(FIVE, DIAMONDS), hand(QUEEN, CLUBS), hand(SIX, CLUBS)));
    }

    @Test
    public void find_a_winner() {
        final List<String> paths = List.of("src/test/resources/Deck.txt",
                "src/test/resources/Deck.txt",
                "src/test/resources/Deck.txt",
                "src/test/resources/Deck.txt");
        final List<String> players = List.of("Player1", "Player2", "Player3", "Player4", "Player5", "Dealer");
        Stream.rangeClosed(1, 1000)
                .forEach(number -> {
                    final List<Player> player = blackJackMultiPlayer.initializeAndPlayGame(paths, players);

                    assertThat(player).isNotEmpty();
                });
    }

    private SuitedCard hand(final Card card, final Suite suite) {
        return new SuitedCard(card, suite);
    }
}