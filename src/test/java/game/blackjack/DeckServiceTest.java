package game.blackjack;

import game.blackjack.domain.DeckService;
import game.blackjack.domain.dtos.Card;
import game.blackjack.domain.dtos.Deck;
import game.blackjack.domain.dtos.Suite;
import game.blackjack.domain.dtos.SuitedCard;
import game.blackjack.errors.Error;
import io.vavr.collection.List;
import io.vavr.control.Either;
import org.junit.Test;

import static game.blackjack.errors.ErrorCodes.INVALID_CONTENT;
import static game.blackjack.errors.ErrorCodes.TOO_MANY_OR_FEW_DECK_FILES;
import static game.blackjack.errors.ErrorCodes.UNABLE_TO_READ_FILE;
import static org.assertj.core.api.Assertions.assertThat;

public class DeckServiceTest {

    private final DeckService deckService = new DeckService();

    @Test
    public void should_always_get_deck_of_52_unique_cards() {
        final Deck deck = deckService.initializeDeck();

        assertThat(deck.getSuitedCards().size()).isEqualTo(52);
        assertThat(deck.getSuitedCards().get(0).getCard()).isEqualTo(Card.TWO);
        assertThat(deck.getSuitedCards().get(0).getSuite()).isEqualTo(Suite.CLUBS);
        assertThat(deck.getSuitedCards().get(51).getCard()).isEqualTo(Card.ACE);
        assertThat(deck.getSuitedCards().get(51).getSuite()).isEqualTo(Suite.SPADES);
    }

    @Test
    public void should_always_get_shuffled_deck_of_52_unique_cards_from_old_deck() {
        final Deck orderedDeck = deckService.initializeDeck();
        final Deck shuffledDeck = deckService.shuffleDeck(orderedDeck);

        assertThat(shuffledDeck.getSuitedCards().size()).isEqualTo(52);
        assertThat(shuffledDeck.getSuitedCards()).isNotEqualTo(orderedDeck);
    }

    @Test
    public void should_always_get_a_new_shuffled_deck_of_52_unique_cards() {
        final Deck shuffledDeck = deckService.shuffleDeck();
        final Deck orderedDeck = deckService.initializeDeck();

        assertThat(shuffledDeck.getSuitedCards().size()).isEqualTo(52);
        assertThat(shuffledDeck.getSuitedCards()).isNotEqualTo(orderedDeck);
    }

    @Test
    public void should_always_get_same_deck_after_removing_a_card() {
        final Deck shuffledDeck = deckService.getShuffledDeckEitherFromFileOrGame(List.of(""), 1);
        final Deck deckAfterDealtHand = deckService.getDeckAfterDealingHands(shuffledDeck);

        assertThat(deckAfterDealtHand.getSuitedCards().size()).isEqualTo(shuffledDeck.getSuitedCards().size() - 1);
        assertThat(deckAfterDealtHand.getSuitedCards()).isEqualTo(shuffledDeck.getSuitedCards().pop());
    }

    @Test
    public void should_always_get_top_card_from_deck() {
        final Deck shuffledDeck = deckService.getShuffledDeckEitherFromFileOrGame(List.of(""), 1);
        final SuitedCard dealtCard = deckService.getDealtCard(shuffledDeck);

        assertThat(dealtCard).isEqualTo(shuffledDeck.getSuitedCards().get(0));
    }

    @Test
    public void should_always_get_a_new_shuffled_deck_either_from_game_or_file_here_from_game() {
        final Deck shuffledDeck = deckService.getShuffledDeckEitherFromFileOrGame(List.of("asdkjvbsd"), 1);

        assertThat(shuffledDeck.getSuitedCards().size()).isEqualTo(52);
    }

    @Test
    public void should_always_get_a_new_shuffled_deck_either_from_game_or_file_here_from_file() {
        final Deck shuffledDeck = deckService.getShuffledDeckEitherFromFileOrGame(List.of("src/test/resources/Deck.txt"), 1);

        assertThat(shuffledDeck.getSuitedCards().size()).isEqualTo(52);
    }

    @Test
    public void should_get_error_for_invalid_file_path() {
        final Either<Error, Deck> deckFromFile = deckService.getDeckFromFile("a;dkljvblasdkjbcalsdjbclasd");

        assertThat(deckFromFile.isLeft()).isTrue();
        assertThat(deckFromFile.isRight()).isFalse();
        assertThat(deckFromFile.getLeft().code).isEqualTo(UNABLE_TO_READ_FILE.code);
        assertThat(deckFromFile.getLeft().message).isEqualTo(UNABLE_TO_READ_FILE.message);
    }

    @Test
    public void should_get_error_for_empty_file_path() {
        final Either<Error, Deck> deckFromFile = deckService.getDeckFromFile("");

        assertThat(deckFromFile.isLeft()).isTrue();
        assertThat(deckFromFile.isRight()).isFalse();
        assertThat(deckFromFile.getLeft().code).isEqualTo(UNABLE_TO_READ_FILE.code);
        assertThat(deckFromFile.getLeft().message).isEqualTo(UNABLE_TO_READ_FILE.message);
    }

    @Test
    public void should_get_error_for_empty_deck_file() {
        final Either<Error, Deck> deckFromFile = deckService.getDeckFromFile("src/test/resources/DeckEmpty.txt");

        assertThat(deckFromFile.isLeft()).isTrue();
        assertThat(deckFromFile.isRight()).isFalse();
        assertThat(deckFromFile.getLeft().code).isEqualTo(INVALID_CONTENT.code);
        assertThat(deckFromFile.getLeft().message).isEqualTo(INVALID_CONTENT.message);
    }

    @Test
    public void should_get_error_for_corrupted_deck_file() {
        final Either<Error, Deck> deckFromFile = deckService.getDeckFromFile("src/test/resources/DeckCorrupted.txt");

        assertThat(deckFromFile.isLeft()).isTrue();
        assertThat(deckFromFile.isRight()).isFalse();
        assertThat(deckFromFile.getLeft().code).isEqualTo(INVALID_CONTENT.code);
        assertThat(deckFromFile.getLeft().message).isEqualTo(INVALID_CONTENT.message);
    }

    @Test
    public void should_get_error_for_duplicated_in_deck_file() {
        final Either<Error, Deck> deckFromFile = deckService.getDeckFromFile("src/test/resources/DeckDuplicates.txt");

        assertThat(deckFromFile.isLeft()).isTrue();
        assertThat(deckFromFile.isRight()).isFalse();
        assertThat(deckFromFile.getLeft().code).isEqualTo(INVALID_CONTENT.code);
        assertThat(deckFromFile.getLeft().message).isEqualTo(INVALID_CONTENT.message);
    }

    @Test
    public void should_get_error_if_deck_size_is_not_52() {
        final Either<Error, Deck> deckFromFile = deckService.getDeckFromFile("src/test/resources/DeckFewCards.txt");

        assertThat(deckFromFile.isLeft()).isTrue();
        assertThat(deckFromFile.isRight()).isFalse();
        assertThat(deckFromFile.getLeft().code).isEqualTo(INVALID_CONTENT.code);
        assertThat(deckFromFile.getLeft().message).isEqualTo(INVALID_CONTENT.message);
    }

    @Test
    public void should_get_error_if_a_card_is_corrupted() {
        final Either<Error, Deck> deckFromFile = deckService.getDeckFromFile("src/test/resources/DeckCorruptedCards.txt");

        assertThat(deckFromFile.isLeft()).isTrue();
        assertThat(deckFromFile.isRight()).isFalse();
        assertThat(deckFromFile.getLeft().code).isEqualTo(INVALID_CONTENT.code);
        assertThat(deckFromFile.getLeft().message).isEqualTo(INVALID_CONTENT.message);
    }

    @Test
    public void should_get_deck_of_unique_cards() {
        final Either<Error, Deck> deckFromFile = deckService.getDeckFromFile("src/test/resources/Deck.txt");

        assertThat(deckFromFile.isRight()).isTrue();
        assertThat(deckFromFile.get().getSuitedCards().size()).isEqualTo(52);
        assertThat(deckFromFile.get().getSuitedCards().get(0).getCard()).isEqualTo(Card.TWO);
        assertThat(deckFromFile.get().getSuitedCards().get(0).getSuite()).isEqualTo(Suite.CLUBS);
        assertThat(deckFromFile.get().getSuitedCards().get(51).getCard()).isEqualTo(Card.ACE);
        assertThat(deckFromFile.get().getSuitedCards().get(51).getSuite()).isEqualTo(Suite.SPADES);
    }

    @Test
    public void should_get_four_ordered_decks() {
        final Deck decksFromGame = deckService.getDecksFromGame(4);

        assertThat(decksFromGame.getSuitedCards().size()).isEqualTo(208);
    }

    @Test
    public void should_get_four_decks_from_file() {
        final Either<Error, Deck> decksFromFile = deckService.getDecksFromFile(List.of("src/test/resources/Deck.txt",
                "src/test/resources/Deck.txt",
                "src/test/resources/Deck.txt",
                "src/test/resources/Deck.txt"),
                4);

        assertThat(decksFromFile.isRight()).isTrue();
        assertThat(decksFromFile.get().getSuitedCards().size()).isEqualTo(208);
    }

    @Test
    public void should_get_error_if_files_are_not_four() {
        final Either<Error, Deck> decksFromFile = deckService.getDecksFromFile(List.of("src/test/resources/Deck.txt",
                "src/test/resources/Deck.txt",
                "src/test/resources/Deck.txt"),
                4);
        final Error expectedError = new Error(TOO_MANY_OR_FEW_DECK_FILES.code, TOO_MANY_OR_FEW_DECK_FILES.message);

        assertThat(decksFromFile.isRight()).isFalse();
        assertThat(decksFromFile.isLeft()).isTrue();
        assertThat(decksFromFile.getLeft().code).isEqualTo(expectedError.code);
        assertThat(decksFromFile.getLeft().message).isEqualTo(expectedError.message);
    }

    @Test
    public void should_get_error_if_cards_in_file_are_corrupt_or_not_present() {
        final Either<Error, Deck> decksFromFile = deckService.getDecksFromFile(List.of("src/test/resources/Deck.txt",
                "src/test/resources/DeckCorrupted.txt",
                "src/test/resources/DeckDuplicates.txt",
                "src/test/resources/DeckFewCards.tt"),
                4);
        final Error expectedError = new Error(INVALID_CONTENT.code, INVALID_CONTENT.message);

        assertThat(decksFromFile.isRight()).isFalse();
        assertThat(decksFromFile.isLeft()).isTrue();
        assertThat(decksFromFile.getLeft().code).isEqualTo(expectedError.code);
        assertThat(decksFromFile.getLeft().message).isEqualTo(expectedError.message);
    }
}